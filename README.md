# Example - Chromium headless with Puppeteer
The repository demonstrates an example using Puppeteer with Chromium for headless browser testing in Bitbucket Pipelines.

Installing puppeteer (via npm and a dependency defined inside package.json) also downloads and installs Chromium, however to work correctly 
when running inside the node:latest docker image, we also need to install a few additional libraries.

## Example
The sample code uses puppeteer to launch a headless Chromium browser instance, navigate to http://example.com, take a screenshot, and 
attach the screenshot as a build artifact. 

## Resources
* [Puppeteer](https://github.com/GoogleChrome/puppeteer) 
* [Troubleshooting - Running Puppeteer in Docker](https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md)
* [Chromium](https://www.chromium.org/)
