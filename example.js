// Adapted from https://github.com/GoogleChrome/puppeteer
const puppeteer = require('puppeteer');

(async () => {
    // Since we are running as root, we need to pass the --no-sandbox arg.
    const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
    const page = await browser.newPage();
    await page.goto('https://example.com');
    await page.screenshot({path: 'example.png'});
    await browser.close();
})();
